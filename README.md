# README #

### Thought Process ###

ASSUMPTION: All data given in csv is correct format
UNSURE: If web interface is used more than once to upload csv, does previous data get overwritten and stick with one schedule or is new csv data added to database and multiple schedules are created and displayed.
ASSUMPTION DUE TO UNCERTAINTY: only most up to date schedule matters so go with overwriting previous data when new csv is uploaded. Resulting in the current being the only one on display.

INITIAL PLAN OF ATTACK: Once csv info is imported into databse take the following steps to create a full schedule (restrictions are not being applied besides not generating a booking past closing time):
                1. Go through each row and create a booking for the schedule using the number_of_time_slots field
                2. For each time slot add an additional 20 min based on previous time slot (automatically move to next row if new time slot will be past closing time)
                3. Once time slot is obtained, create a hash based on sample output. ({user_email: .... , datetime: .... , club_name: ...})
                4. Add hash to a full_schedule array

                Once we have the full_schedule, we apply the restrictions and return two separate hash arrays.
                Successful array of hashes will contain the same hash keys as full_schedule array
                Unsuccessful array of hashes needs to have same layout as TeeTimeBooking row (didn't keep that in mind, will make it difficult to use this approach)

AlTERNATE PLAN OF ATTACK: add data to successful array or unsuccessful array as we go through each row one by one, generating all bookings for each row based on restrictions. Make sure correct format is displayed on view.

***Still need to way out pros and cons of approaches in an indepth fashion. Code may not be as readable since refacotring needs to be done


### How do I get set up? ###

Using Rails 5.0.2 and Ruby 2.3.0

after you create a local copy of project run "bundle install" then "rake db:migrate"

"rails server", will start up the app.

access the web app on your browser at "localhost:3000"

