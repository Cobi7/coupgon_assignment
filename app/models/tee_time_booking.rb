class TeeTimeBooking < ApplicationRecord

  OPENING_TIME = 9
  CLOSING_TIME = 17

  def self.import(file)
    if File.extname(file.original_filename) === ".csv"
      CSV.foreach(file.path, headers: true) do |row|
        TeeTimeBooking.create! row.to_hash
      end
    else
       raise "Unknown file type: #{file.original_filename}"
    end
  end

#logic should be moved to control and logic needs to be modified based on approach
  private

  def booking_during_operating_hours?(booking)
    booking.start_date.hour >= OPENING_TIME && booking.start_date.hour < CLOSING_TIME ? true : false
  end
end


# A golf club is only open for bookings from 9am-5pm each day.    fail booking if outside of hours
# Tee times are separated by 20 minutes.  use where query to see if a booking exists at that time
# Users may not book tee times within 3 hours of each other. use where query to see if other bookings against for user at club
# A user may not schedule more than 1 tee time at more than 1 club within a 3 hour period.
# Only one user may book a given twenty-minute slot at a club.
# The UI should display a list of users and bookings that were unable to be completed (due to e.g. overflow, constraint violation, etc.)