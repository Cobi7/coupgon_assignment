class TeeTimeBookingsController < ApplicationController
  def index
    @bookings = TeeTimeBooking.all
    @full_schedule = schedule(@bookings)
  end

  def import
    TeeTimeBooking.import(params[:file])
    redirect_to root_url, notice: "Products imported."
  end

  private

  def schedule(bookings)
    #needs to be refactored, current flow needs to be changed to account for over one hour time slot differences.
    #Also move logic into different method
    full_schedule = []
    new_min = 0
    new_hour = 0
    bookings.each do |booking_collection|
      new_min = 0
      new_hour = 0
      booking_collection.number_time_slots.times do
            byebug
        iteration = 20*new_min
        if iteration >= 60
          new_hour +=1
          iteration = 60 - iteration
        end
        start_date = booking_collection.start_date
        booking_date = start_date.change({ hour: start_date.hour+new_hour, min: start_date.min+iteration })
        if booking_date > booking_collection.end_date
          break
        end
        booking = {user_email: booking_collection.user_email, dateime: booking_date, club_name: booking_collection.club_name }
        full_schedule << booking
        new_min+=1
      end
    end
    full_schedule
    #utilize restriction methods for final results
  end

  #implement restriction methods:
      #if booking does not work with operating hours then fail
      #if booking can be made 3 hours apart from each other by the same user regardless of club then good
      #if booking of one user is not 20 min apart from another user's booking at the same club then fail

end
